var gulp = require('gulp');
var browserify = require('gulp-browserify');
var concat = require('gulp-concat');
var less = require('gulp-less');
var refresh = require('gulp-livereload');
var minifyCSS = require('gulp-minify-css');
var embedlr = require('gulp-embedlr');
var plumber = require('gulp-plumber');
var lr = require('tiny-lr');
var server = lr();

gulp.task('styles', function() {
    gulp.src(['less/living-styleguide.less'])
        .pipe(plumber())
        .pipe(less())
        .pipe(minifyCSS())
        .pipe(gulp.dest('assets/css'))
        .pipe(refresh(server))
})

gulp.task('html', function() {
    gulp.src(['index.html'])
        .pipe(embedlr())
        .pipe(refresh(server))
})
 

gulp.task('watch-less', ['styles', 'html'], function() {
 
    gulp.watch('less/**', function(event) {
        gulp.run('styles');
        gulp.run('html');
    })

})